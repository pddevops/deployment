# Deployment to production

The script deploy_on_production.sh is used for automatic deployment on production servers:

  - Processing Production server : ankit@104.154.115.26
  - Apis Production server : ahwan@104.154.19.250
  - Frontend(web.paralleldots.com): ahwan@146.148.47.82 

The script pulls the code from Git Repo on to the Processing Production Server, Apis Production server and frontend server
 and restarts the files running, to include the changes made to the file.   

## Processing Server Files Running:
### Facebook:
	- CRON: /home/ankit/facebook_batch/pd_fbcron.py
### News:
	- CRON: /home/ankit/news_batch/news_batch_processing_cron.py
	- SUPERVISOR(news2_init): /home/ankit/news_batch/initial_news_dump_RabbitMQ.py
### Twitter:
	- CRON: /home/ankit/twitter_batch/twitter_tracking.py
	- CRON: /home/ankit/twitter_batch/tenminutescron.sh
	- CRON: /home/ankit/twitter_batch/tweetDataAnalytics.py
	- SUPERVISOR(filtering_gnip_data): /home/ankit/twitter_batch/semantic_twitter_analysis.py

## Apis Server Files Running:
### KARNA:
	- SUPERVISOR(karnaapis): /home/ahwan/service/karnaservice.py
	
## Frontend:
	- Path: /home/ahwan/staging/tracking/

